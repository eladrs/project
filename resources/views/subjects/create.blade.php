@extends('layouts.app')
@section('content')

<div class ="card card -default">
    <div class="card-header">
    {{isset($subject)? "Edit Subject " :"Create New Subject" }}
    </div>
    <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-group">
            @foreach ($errors->all() as $error)
                <li class="list-group-item text-danger">
                {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
    @endif

   <form method="POST" action="{{isset($subject)?route('subjects.update',$subject->id): route('subjects-store',[$meeting_id])}}">
     {{csrf_field()}}
     @if(isset($subject))
      @method('PUT')
     @endif 

      <div class="form-group">
       <label for="description">Description</label>
       <input type="text" id="description" class="form-control" name="description" value="{{isset($subject) ? $subject->description :''}}">
      </div>

      <div class="form-group">
       <label for="time_start">Time Start </label>
       <input type="datetime-local" id="time_start" class="form-control" name="time_start" value="{{isset($subject) ? $subject->time_start :''}}"required>
      </div>    

      <div class="form-group">
       <label for="time_end">Time End </label>
       <input type="datetime-local" id="time_end" class="form-control" name="time_end" value="{{isset($subject) ? $subject->time_end :''}}"required>
      </div> 

      <div class="form-group">
       <button class="btn btn-success">
       {{isset($subject)?'Update Subject' : 'Add New Subject'}}
       </button>
      </div>
     </form>
   </div>
</div>    
@endsection