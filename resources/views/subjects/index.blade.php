@extends('layouts.app')
@section('content')

<div class ="d-flex justify-content-end mb-2">
    <a href ="{{route('subjects.create')}}"class="btn btn-success">Add New Subject</a>
</div>

<div class ="card card -default">
    <div class="card-header">subjects</div>
     <div class = "card-body">
      <table class="table">
       <thead>
       <th>Subjects Description</th>
        <th>Subjects Start</th>
        <th>Subjects End</th>
        <th>Status</th>
        <th></th>
       </thead>

       <tbody>
        @foreach($subjects as $subject)
         <tr>
          <td>{{$subject->description}}</td>
          <td>{{$subject->subject_start}}</td>
          <td>{{$subject->subject_end}}</td>
          <td>{{$subject->status}}</td>
          <td> <a href ="{{route('subjects.edit',$subject->id)}}" class="btn btn-info btn-sm">Edit</a></td>
          </tr>
        @endforeach 
       </tbody>
      </thead>
            
     </div>   
</div>
@endsection
