@extends('layouts.app')
@section('content')
    
    
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8 mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
                                <h2 class="text-black mb-0">Organization meetings details</h2>
                            </div>
                            <div class="col">
                                <ul class="text-uppercase text-muted ls-1 mb-1">
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        
                    <table class="table">
       <thead>
        <th>Meeting Title</th>
        <th>Number Of invites</th>
        <th>Num of task </th>
        <th>Num of task that done</th>
        <th></th>
        
       </thead>
       <tbody>
        @foreach($dashboard as $dashbaord)
         <tr>
         <td>{{$dashbaord->meeting_title}}</td>
          <td>{{$dashbaord->num_users}}</td>
          <td>{{$dashbaord->num_tasks}}</td>
          <td>{{$dashbaord->num_tasks_done}}</td>
         </tr>
        @endforeach 
       </tbody>
      </table>                        





                        <div class="chart">
                        
                            <canvas id="chart-sales" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-muted ls-1 mb-1">My private tasks</h6>
                                <h2 class="mb-0">undone tasks</h2>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                    <table class="table">
       <thead>
       
        <th></th>
        
       </thead>
       <tbody>
        @foreach($tasks as $task)
         <tr>
         <td><a href ="{{route('meetingTasks',$task->meeting_id)}}">{{$task->title}}</a></td>
        @endforeach 
       </tbody>
      </table>
                       

    </div>



    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush