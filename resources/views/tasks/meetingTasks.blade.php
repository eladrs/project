@extends('layouts.app')
@section('content')
<div class="container">
@cannot('participant')
<div class ="d-flex justify-content-end mb-2">
 <a href ="{{route('subjects.create',['meeting_id'=>$meeting_id])}}" class="btn btn-success">Add New Subject</a>
</div>
@endcannot
<h5><span class="badge badge-dark">Meeting number : {{$meeting_id}}</span></h5><br>
<div class ="card card -default">
    <div class="card-header">subject</div>
     <div class = "card-body">
      <table class="table">
       <thead>
        <th>Subjects Description</th>
        <th>Subjects Start</th>
        <th>Subjects End</th>
        @cannot('participant')
        <th>Status</th>
        @endcannot
        <th></th>
        <th></th>
        <th></th>
       </thead>

       <tbody>
       <?php
          date_default_timezone_set('Asia/Jerusalem')
       ?>
        @foreach($subjects as $subject)
         <tr>
          <td>{{$subject->description}}</td>
          <td>{{$subject->time_start}}</td>
          <td>{{$subject->time_end}}</td>
          @cannot('participant')
          <td>
            @if(($subject->status==0)&(date('Y-m-d H:i:s') > $subject->time_start)&(date('Y-m-d H:i:s') < $subject->time_end))
              <a href="{{route('discussed',$subject->id)}}">Mark As discussed</a>
            @elseif (($subject->status==0)&(date('Y-m-d H:i:s') > $subject->time_end))
             <font color="red">Subject Not Discussed</font>
            @elseif($subject->status==1)
            <font color="green">Subject Discussed</font>
              
            @else
              Meeting has not started
            @endif
            </td>
            @endcannot
          <td><a href ="{{route('subjects.edit',$subject->id)}}" class="btn btn-info btn-sm">Edit</a></td>
          @cannot('participant')
    <td><a href ="{{route('moveTo',$subject->id)}}" class="btn btn-info btn-sm">move to other meeting</a></td>
    @endcannot
          <td>
          <form method="post" action ="{{action('SubjectController@destroy',$subject->id)}}">
          @csrf
          @method('DELETE')
            <input type ="submit" class=" btn btn-danger btn-sm" name="submit" value ="Delete"> 
          </form>
          </td>
         </tr>
        @endforeach 
        </tbody>
      </table>
                
     </div>   
    </div><br><br>
@cannot('participant')  
<div class ="d-flex justify-content-end mb-2">
    <a href ="{{route('tasks.create',['meeting_id'=>$meeting_id])}}"class="btn btn-success">Add New Task</a>
</div>
@endcannot

<div class ="card card -default">
    <div class="card-header">tasks</div>
     <div class = "card-body">
      <table class="table">
       <thead>
        <th>Task Title</th>
        <th>Task start</th>
        <th>Task end</th>
        <th>Status</th>
        <th></th>
        <th></th>
        <th></th>
       </thead>

       <tbody>
        @foreach($tasks as $task)
         <tr>
          <td>{{$task->title}}</td>
          <td>{{$task->task_start}}</td>  
          <td> {{$task->task_end}} </td>
          <td>
            @if (($task->status==0)&(date('Y-m-d H:i:s') < $task->task_end) )
              <a href="{{route('done',$task->id)}}">Mark As done</a>
            @elseif(($task->status==0)&(date('Y-m-d H:i:s') > $task->task_end) )
              <a href="{{route('donelate',$task->id)}}">Mark As done late </a>
            @elseif($task->status==2)
             <font color="red">Task was completed late</font>
            @else
              Task Done.
            @endif
          </td>
          <td><a href ="{{route('tasks.edit',$task->id)}}" class="btn btn-info btn-sm">Edit</a></td>
          <td>
          <form method="post" action ="{{action('TaskController@destroy',$task->id)}}">
          @csrf
          @method('DELETE')
            <input type ="submit" class=" btn btn-danger btn-sm" name="submit" value ="Delete"> 
          </form>
          </td>
         </tr>
        @endforeach 
       </tbody>
      </table>
            
     </div>   
</div>
</div>
@endsection

