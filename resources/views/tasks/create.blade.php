@extends('layouts.app')
@section('content')

<div class ="card card -default">
    <div class="card-header">
    {{isset($task)? "Edit Task " :"Create New Task" }}
    </div>
    <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-group">
            @foreach ($errors->all() as $error)
                <li class="list-group-item text-danger">
                {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
    @endif
   <form method="POST" action="{{isset($task)?route('tasks.update',$task->id) : route('tasks-store',[$meeting_id])}}">
     {{csrf_field()}}
     @if(isset($task))
      @method('PUT')
     @endif 


      <div class="form-group">
       <label for="title">Task</label>
       <input type="text" id="title" class="form-control" name="title" value="{{isset($task) ? $task->title :''}}">
      </div> 
      
        {{-- <input type="hidden" value="{{$meeting_id}}" name="meeting_id_from_hidden_input"> --}}
      
      <div class="form-group">
       <label for="task_user_id">Responsible Task (id of employees)</label>
       <input type="number" id="task_user_id" class="form-control" name="task_user_id" value="{{isset($task) ? $task->task_user_id :''}}" required>
      </div> 
      
      <div class="form-group">
       <label for="task_start">Task Start </label>
       <input type="datetime-local" id="task_start" class="form-control" name="task_start" value="{{isset($task) ? $task->task_start :''}}"required >
      </div>    

      <div class="form-group">
       <label for="task_end">Task End </label>
       <input type="datetime-local" id="task_end" class="form-control" name="task_end" value="{{isset($task) ? $task->task_end :''}}"required >
      </div> 

      <div class="form-group">
       <button class="btn btn-success">
       {{isset($task)?'Update Task' : 'Add Task'}}
       </button>
      </div>
     
     </form>
   </div>
</div>    
@endsection