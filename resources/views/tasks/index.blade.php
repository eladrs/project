@extends('layouts.app')
@section('content')
@cannot('participant')
<div class ="d-flex justify-content-end mb-2">
    <a href ="{{route('tasks.create')}}"class="btn btn-success">Add New Task</a>
</div>
@endcannot
<div class ="card card -default">
    <div class="card-header">tasks</div>
     <div class = "card-body">
      <table class="table">
       <thead>
        <th>Task Title</th>
        <th>Meeting Name</th>
        <th>Task start</th>
        <th>Task end</th>
        <th>Status</th>
        <th></th>
        
       </thead>
       <tbody>
        @foreach($tasks as $task)
        @if ((Auth::user()->id)==($task->task_user_id))
         <tr>
         <td>{{$task->title}}</td>
          <td>{{$task->meeting_id}}</td>
          <td>{{$task->task_start}}</td>
          <td>{{$task->task_end}}</td>
          <td>
            @if (($task->status==0)&(date('Y-m-d H:i:s') < $task->task_end) )
              <a href="{{route('done',$task->id)}}">Mark As done</a>
            @elseif(($task->status==0)&(date('Y-m-d H:i:s') > $task->task_end) )
              <a href="{{route('donelate',$task->id)}}">Mark As done late </a>
            @elseif($task->status==2)
             <font color="red">Task was completed late</font>
            @else
              Task Done.
            @endif
          </td>
          @cannot('participant')
          <td> <a href ="{{route('tasks.edit',$task->id)}}" class="btn btn-info btn-sm">Edit</a></td>
         @endcannot
          </tr>
        
        @endif  
        @endforeach 
       </tbody>
      </thead>
            
     </div>   
</div>
@endsection
