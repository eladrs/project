@extends('layouts.app')
@section('content')


<form>
    <div class ="container">
      <div class="col-2 offset-10">
          <a href="{{route('employees.index')}}" class=" form-control btn btn-primary">Back to list</a>
      </div>
  </div>
</form>


<div class="container">
  <h3>change role</h3>
    <form method="post" action ="{{action('EmployeesController@update',$user->id)}}">
     @csrf
     @method('PATCH')
     <div class="form-group">
        <select name="role" class="form-control">
            <option value =admin>Admin</option>
            <option value =manger>Manger</option>
            <option value =participant>Participant</option>
        </select>
     </div>

    
     <div class ="container">
        <div class="col-4  offset-4">
                <input type ="submit" class="form-control btn btn-secondary" name="submit" value ="Save Update">
        </div>
     </div>
    </form>
</div><br>

@endsection