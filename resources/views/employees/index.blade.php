@extends('layouts.app')
@section('content')


<div class ="card card -default">
    <div class="card-header">emloyees</div>
     <div class = "card-body">
      <table class="table">
       <thead>
        <th>user name</th>
        <th>role</th> 
        <th></th><th></th>
       </thead>
       <tbody>

        @foreach($users as $user)
         <tr>
         <td>{{$user->name}}</td>
          <td>{{$user->role}}</td>
          <td>
           <a href ="{{route('employees.edit',$user->id)}}" class="btn btn-info btn-sm">Edit</a>
          </td>
           <td>
           <form method="post" action ="{{action('EmployeesController@destroy',$user->id)}}">
            @csrf
            @method('DELETE')
            <input type ="submit" class=" btn btn-danger btn-sm" name="submit" value ="Delete"> 
           </form>
          </td>
         </tr>
        @endforeach 
       </tbody>
      </table>
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <form action="" method="POST" id=deleteMeetingForm>
                @csrf
                @method('DELETE')
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Meeting</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center text-bold">
                        Are you sure you want to delete this meeting?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Go Back</button>
                    <button type="submit" class="btn btn-danger">Yes, Delete</button>
                </div>
                </div>
            </form>
            </div>
            </div>
     </div>   
</div>
@endsection