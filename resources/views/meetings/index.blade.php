@extends('layouts.app')
@section('content')

@if(!empty($successMsg))
  <div class="alert alert-success"> {{ $successMsg }}</div>
@endif
<div class ="d-flex justify-content-end mb-2">
@cannot('participant')
    <a href ="{{route('meetings.create')}}" class="btn btn-success">Add New Meeting</a>
@endcannot  
</div>

<div class ="card card -default">
    <div class="card-header">meeting</div>
     <div class = "card-body">
      <table class="table">
       <thead>
        <th>Meeting Title</th>
        <th>Meeting ID</th>
        <th>Seating times</th>
        <th></th>
        
       </thead>
       <tbody>
        @foreach($meetings as $meeting)
         <tr>
         <td><a href ="{{route('meetingTasks',$meeting->id)}}">{{$meeting->title}}</a></td>
          <td>{{$meeting->id}}</td>
          <td>{{$meeting->meeting_start}}</td>
          <td>
           <a href ="{{route('meetings.edit',$meeting->id)}}" class="btn btn-info btn-sm">Edit</a>
           <button class="btn btn-danger btn-sm" onclick="handleDelete({{$meeting->id}})">Delete</button>
          </td>
         </tr>
        @endforeach 
       </tbody>
      </table>
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <form action="" method="POST" id=deleteMeetingForm>
                @csrf
                @method('DELETE')
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Meeting</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center text-bold">
                        Are you sure you want to delete this meeting?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Go Back</button>
                    <button type="submit" class="btn btn-danger">Yes, Delete</button>
                </div>
                </div>
            </form>
            </div>
            </div>
     </div>   
</div>
@endsection

@section('scripts')
 <script>
  function handleDelete(id){
      var form = document.getElementById("deleteMeetingForm");
      form.action= 'meetings/'+ id; 
      $('#deleteModal').modal('show')
  }
 </script>
@endsection