@extends('layouts.app')
@section('content')



<div class ="card card -default">
     <div class = "card-body">
      <table class="table">
       <thead>
        <th>Meeting Id</th>
        <th></th>
        
       </thead>
       <tbody>
        @foreach($meetings as $meeting)
         <tr>
         <td><a href ="{{route('meetingTasks',$meeting->id)}}">{{$meeting->meeting_id}}</a></td>
         </tr>
        @endforeach 
       
</div>
@endsection

@section('scripts')
 <script>
  function handleDelete(id){
      var form = document.getElementById("deleteMeetingForm");
      form.action= 'meetings/'+ id; 
      $('#deleteModal').modal('show')
  }
 </script>
@endsection