@extends('layouts.app')
@section('content')

<div class ="card card -default">
    <div class="card-header">
    {{isset($meeting)? "Edit Meeting Category" :"Create Meeting Category" }}
    </div>
    <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-group">
            @foreach ($errors->all() as $error)
                <li class="list-group-item text-danger">
                {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
    @endif

   <form method="POST" action="{{isset($meeting)?route('meetings.update',$meeting->id) : route('meetings.store')}}">
     {{csrf_field()}}
     @if(isset($meeting))
      @method('PUT')
     @endif 
      <div class="form-group">
       <label for="title">Title</label>
       <input type="text" id="title" class="form-control" name="title" value="{{isset($meeting) ? $meeting->title :''}}">
      </div> 
      <div class="form-group">
       <label for="meeting_start">Meeting Start </label>
       <input type="datetime-local" id="meeting_start" class="form-control" name="meeting_start" value="{{isset($meeting) ? $meeting->meeting_start :''}}" required>
      </div>    

      <div class="form-group">
       <label for="meeting_end">Meeting End </label>
       <input type="datetime-local" id="meeting_end" class="form-control" name="meeting_end" value="{{isset($meeting) ? $meeting->meeting_end :''}}"required>
      </div> 

      <div class="form-group">
      <label for="choose">choose usets that you want to invite (please press Ctrl and mark the name)</label>

         <select  class="form-control" data-role="tagsinput"  name="inviteds[]" multiple="multiple">
                                            <option value="{{Auth::user()->id}}" selected>me</option>
                                    
                                    @foreach($users as $user)
                                            @if($user->organization_id==Auth::user()->organization_id && $user->id!=Auth::user()->id)                            
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endif
                                     @endforeach
                                    </select>
      </div>

      <div class="form-group">
       <button class="btn btn-success">
       {{isset($meeting)?'Update Meeting' : 'Add Meeting'}}
       </button>
      </div>
     </form>
   </div>
</div>    
@endsection