<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDashboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboards', function (Blueprint $table) {
             
                $table->increments('id');
                $table->string('meeting_title');
                $table->bigInteger('num_users')->unsigned()->nullable();
                $table->bigInteger('num_tasks')->unsigned()->nullable();
                $table->bigInteger('num_tasks_done')->unsigned()->nullable();
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboards');
    }
}