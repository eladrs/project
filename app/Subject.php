<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable =[
        'time_start','time_end','description','status'
    ];
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function meeting(){
        return $this->belongsTo('App\Meeting');
    }

}
