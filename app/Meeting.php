<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable =[
        'title','meeting_start','meeting_end' ];

        public function user(){
            return $this->hasMany('App\User');
        }
    
        public function meeting(){
            return $this->hasMany('App\Meeting');
        }
}
