<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable =[
        'title','task_user_id','task_start','tsak_end'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function meeting(){
        return $this->belongsTo('App\Meeting');
    }

}

