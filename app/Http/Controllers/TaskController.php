<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use App\User;
use App\Task;
use App\Subject;
use App\Organization;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Tasks\CreatTaskRequest;
use App\Http\Requests\Tasks\UpdateTasksRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;


class TaskController extends Controller
{
    
    public function index()
    {
     return view('tasks.index')->with ('tasks',Task::all());   
    }

    public function meetingTasks($meeting_id)
    {
       $id=$meeting_id;
       $subjects = Subject::where('meeting_id', $id)->get();
       $tasks = Task::where('meeting_id', $id)->get();
       return view('tasks.meetingTasks',compact('tasks', 'subjects', 'meeting_id'));
    }   

    
    public function create(Request $request)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to create new Task..");
        }
        $meeting_id = $request->get('meeting_id');
        return view('tasks.create')->with('meeting_id', $meeting_id);
    }

    public function store($meeting_id, Request $request)
    {
        //dd($request);
        $task=new Task();
        $id = Auth::id();
        $task->title=$request->title;
        $task->task_user_id=$request->task_user_id;
        $task->meeting_id= $meeting_id;
        $task->task_start=$request->task_start;
        $task->task_end=$request->task_end;
        $task->status=0;
        $task->save();
        $subjects = Subject::where('meeting_id', $meeting_id)->get();
        $tasks = Task::where('meeting_id', $meeting_id)->get();
        return redirect(route('meetingTasks',[$meeting_id] ));
         
    }
 
   
    public function show($id)
    {
        //
    }

    
    public function edit(Task $task)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to edit this Task..");
        }
        return view('tasks.create')->with('task', $task);
        
    }
    
    public function update(UpdateTasksRequest $request,Task $task)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to update this Task..");
        }
        $task->title=$request->title;
        $task->save();
        return redirect(route('tasks.index')); 
    }

    
    public function destroy(Task $task)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to delete this Task..");
        }
        $task->delete();
        return redirect(route('meetings.index'));
    }
    public function done($id)
    { 
        
        $task = Task::findOrFail($id);
       // dd($task)    ;        
        $task->status = 1; 
        $task->save();
        $meeting_id=$task->meeting_id;
        $subjects = Subject::where('meeting_id',$meeting_id)->get();
        $tasks = Task::where('meeting_id', $meeting_id)->get();
        return view('tasks.meetingTasks', compact('tasks','subjects','meeting_id')); 
    }
    public function donelate($id)
    { 
        $task = Task::findOrFail($id);
       // dd($task)    ;        
        $task->status = 2; 
        $task->save();
        $meeting_id=$task->meeting_id;
        $subjects = Subject::where('meeting_id',$meeting_id)->get();
        $tasks = Task::where('meeting_id', $meeting_id)->get();
        return view('tasks.meetingTasks', compact('tasks','subjects','meeting_id')); 
    }
    public function discussed($id)
    { 
        
        $subject = Subject::findOrFail($id);
       // dd($task)    ;        
        $subject->status = 1; 
        $subject->save();
        $meeting_id=$subject->meeting_id;
        $subjects = Subject::where('meeting_id',$meeting_id)->get();
        $tasks = Task::where('meeting_id', $meeting_id)->get();
        return view('tasks.meetingTasks', compact('tasks','subjects','meeting_id')); 
    }
    

    
    /*public function destroy($id)
    {
        $task = Task::find($id);
        $meeting_id=$task->meeting_id;
        $subjects = Subject::where('meeting_id',$meeting_id)->get();
        $tasks = Task::where('meeting_id',$meeting_id)->get();
        $task->delete();
        //dd($meeting_id);
        return view('tasks.meetingTasks', compact('tasks','subjects','meeting_id')); 
    }*/
}