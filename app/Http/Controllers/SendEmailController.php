<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Gate;

if (Gate::allows('admin')) {
    abort(403,"Sorry you are not allowed to send mails..");
}
class SendEmailController extends Controller
{
    
    function index()
    {
     return view('send_email');
    }

    function send(Request $request)
    
    {
     $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'message' =>  'required'
     ]);

        $data = array(
            'name'      =>  $request->name,
            'message'   =>   $request->message
        );
        $mailing = array(
            'email'      =>  $request->email
        );

     Mail::to($mailing)->send(new SendMail($data));
     return back()->with('success', 'Thanks for contacting us!');

    }
}

?>