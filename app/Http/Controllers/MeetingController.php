<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use App\User;
use App\Invite;
use App\Task;
use App\Subject;
use App\Organization;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Meetings\CreatMeetingRequests;
use App\Http\Requests\Meetings\UpdateMeetingsRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class MeetingController extends Controller
{
    
    public function index()
    {
        $id_user = Auth::id();
        $user= User::where('id', $id_user)->firstOrFail();
        $organization_id=$user->organization_id;
        //return view('meetings.index')->with ('meetings',Meeting::all());
        return view('meetings.index')->with ('meetings',Meeting::where('organization_id',$organization_id)->get());
    }
    public function indexMy(){
    
        $id_user = Auth::id();
        $myMettings=Invite::where('user_id', $id_user)->get();
        //return view('meetings.index')->with ('meetings',Meeting::all());
        return view('meetings.myMetting')->with ('meetings',$myMettings);
    }
    

    
    public function create()
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to create new meeting..");
        }
        $id = Auth::id();
        $user= User::find($id);
        $org_id= $user->organization_id; 
        $users = User::where('organization_id',$org_id)->get();

        return view('meetings.create',compact('users'));   
     }

   
    public function store(Request $request)
    {

        $id_user = Auth::id();
        $user= User::where('id', $id_user)->firstOrFail();
        $organization_id=$user->organization_id;
        $meeting=new Meeting();
        $id = Auth::id();
        $meeting->user_id=$id;
        $meeting->organization_id= $organization_id;
        $meeting->title=$request->title;
        $meeting->meeting_start=$request->meeting_start;
        $meeting->meeting_end=$request->meeting_end;
        $meeting->save();

        foreach($request->inviteds as $invited){
            Invite::create([
                'meeting_id'=>$meeting->id,
                'user_id'=>$invited,
            ]);
        }

     
        return redirect(route('meetings.index'));  
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit(Meeting $meeting)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to edit the meeting..");
        }
        $id = Auth::id();
        $user= User::find($id);
        $org_id= $user->organization_id; 
        $users = User::where('organization_id',$org_id)->get();

        return view('meetings.create')->with('meeting', $meeting)->with('users', $users);
    }
    
    public function update(UpdateMeetingsRequest $request,Meeting $meeting)
    {
        $meeting->title=$request->title;
        $meeting->save();
        return redirect(route('meetings.index')); 
    }

    
    public function destroy(Meeting $meeting)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to edit the meeting..");
        }
        $meeting->delete();
        return redirect(route('meetings.index'));
    }
}
