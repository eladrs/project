<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use App\User;
use App\Invite;
use App\Task;
use App\Subject;
use App\Dashboard;
use App\Organization;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Meetings\CreatMeetingRequests;
use App\Http\Requests\Meetings\UpdateMeetingsRequest;
use Illuminate\Support\Facades\DB;
use App\Charts\UserChart;



class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_user = Auth::id();
        $status=0;
        
        $tasks=Task::where('task_user_id',$id_user)->where('status',$status)->get();
        //return view('meetings.index')->with ('meetings',Meeting::all());
        
        $id_user = Auth::id();
        $user= User::where('id', $id_user)->firstOrFail();
        $organization_id=$user->organization_id;

        $meetings = Meeting::where('organization_id',$organization_id)->get();
        
        DB::table('dashboards')->delete();

        foreach ($meetings as $meetings ){  
            $meet_id=$meetings->id;
            $meeting_title=$meetings->title; 
            $num_users=DB::table('invites')->where('meeting_id',$meet_id)->count();
            $num_tasks=Task::where('meeting_id',$meet_id)->count();
            $num_tasks_done=Task::where('meeting_id',$meet_id)->where('status','=',1)->count();

            $dashbord = new Dashboard();
            $dashbord->meeting_title = $meeting_title;
            $dashbord->num_users = $num_users;
            $dashbord->num_tasks =$num_tasks;
            $dashbord->num_tasks_done =$num_tasks_done;
            $dashbord->save();
        }
        $dashboard=Dashboard::all();

        $chart=3;
    
        return view('dashboards.index')->with ('tasks',$tasks)->with ('dashboard',$dashboard)->with ('chart',$chart);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}