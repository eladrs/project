<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use App\User;
use App\Task;
use App\Subject;
use App\Organization;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Subjects\CreatSubjectRequest;
use App\Http\Requests\Subjects\UpdateSubjectsRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;



class SubjectController extends Controller
{
    
    public function index()
    {
        return view('subjects.index')->with ('subjects',Subject::all());
    }

  
    public function create(Request $request)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to create new subjecs..");
        }
        $meeting_id = $request->get('meeting_id');
        return view('subjects.create')->with('meeting_id', $meeting_id);
    }
    public function moveTo($subject_id)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to create new subjecs..");
        }
        $subject = Subject::where('id',$subject_id)->first();
        $meeting_id=$subject->meeting_id;
        $meeting=Meeting::where('id',$meeting_id)->first();
        $organization_id= $meeting->organization_id;
        //dd($organization_id);
       // return view('meetings.moveTo')->with ('meetings',Meeting::all())->with('subject_id',$subject_id);
        return view('meetings.moveTo')->with ('meetings',Meeting::where('organization_id',$organization_id)->get())->with('subject_id',$subject_id);

    }


    public function moveToThisMeeting($meeting_id,$subject_id)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to update the status..");
        }
       $id_subject=$subject_id;
       $id_meeting= $meeting_id;
       $subject = Subject::where('id',$id_subject)->firstOrFail();
       $subject->meeting_id=$id_meeting;
       $subject->save(); 
    
       $id_user = Auth::id();
       $user= User::where('id', $id_user)->firstOrFail();
       $organization_id=$user->organization_id;
       return view('meetings.index')->with ('meetings',Meeting::where('organization_id',$organization_id)->get())->with('successMsg','The subject has been moved  .');
    } 

    
    public function store($meeting_id,Request $request)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to update the status..");
        }
        $subject=new Subject();
        $id = Auth::id();
        $subject->meeting_id= $meeting_id;
        $subject->time_start=$request->time_start;
        $subject->time_end=$request->time_end;
        $subject->description=$request->description;
        $subject->status=0;
        $subject->save();
       
        $subjects = Subject::where('meeting_id', $meeting_id)->get();
        $tasks = Task::where('meeting_id', $meeting_id)->get();
        return redirect(route('meetingTasks',[$meeting_id] ));

    }

   
    public function show($id)
    {
        
    }

   
    public function edit(Subject $subject)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to edit this subjec..");
        }
        return view('subjects.create')->with('subject', $subject);
    }

   
    public function update(UpdateSubjectsRequest $request,Subject $subject)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to update the status..");
        }
        $subject->update($request->except(['_token']));
        if ($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
        $subject->description=$request->description;
        $subject->save();
        return redirect(route('subjects.index')); 
    }

    public function destroy(Subject $subject)
    {
        if (Gate::allows('participant')) {
            abort(403,"Sorry you are not allowed to delete this subjec..");
        }
        $subject->delete();
        return redirect(route('meetings.index'));
    }
    
}
