<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    protected $fillable = [
        'meeting_title', 'meeting_date','num_users','num_tasks','num_tasks_done',
    ];
}
