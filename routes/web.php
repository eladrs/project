<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home','HomeController@index')->name('home');
Route::post('/tasks-store/{meeting_id?}','TaskController@store')->name('tasks-store');
Route::post('/subjects-store/{meeting_id?}','SubjectController@store')->name('subjects-store');
Route::get('tasks/done/{id}', 'TaskController@done')->name('done');
Route::get('tasks/donelate/{id}', 'TaskController@donelate')->name('donelate');
Route::get('tasks/discussed/{id}','TaskController@discussed')->name('discussed');
Route::resource('/employees','EmployeesController')->middleware('auth');
Route::get('/MyMeetings','MeetingController@indexMy')->name('MyMeetings');

Route::resource('/meetings','MeetingController')->middleware('auth');
Route::get('/sendemail', 'SendEmailController@index')->name('sendemail');
Route::post('/sendemail/send', 'SendEmailController@send');

Route::resource('/dashboards','DashboardController')->middleware('auth');

Route::resource('/tasks','TaskController')->except(['store']);
Route::resource('/subjects','SubjectController')->except(['store']);
Route::get('tasks/{meeting_id}','TaskController@store');
Route::get('meetings/{organization_id}','MeetingController@store');
Route::get('subjects/{meeting_id}','SubjectController@store');

Route::get('tasks/meetingTasks/{meeting_id}','TaskController@meetingTasks')->name('meetingTasks');
Route::get('/newOne', function () { return view('newOne');})->name('newOne');

Route::get('/moveTo/{id?}','SubjectController@moveTo')->name('moveTo');
Route::get('/moveToThisMeeting/{id?}/{subject_id}','SubjectController@moveToThisMeeting')->name('moveToThisMeeting');